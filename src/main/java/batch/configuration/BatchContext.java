package batch.configuration;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import java.sql.Connection;

/**
 * Created by philippe local on 2017-08-20.
 */
@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan("batch")
@EnableBatchProcessing
public class BatchContext {


    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(name="transactionManager")
    public ResourcelessTransactionManager createResourceLessTransactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public DataSourceInitializer createDataSourceInitializer(DriverManagerDataSource dataSource,
                                                             @Value("${destroyScript}") Resource destroyScript,
                                                             @Value("${initScript}") Resource initScript) {
        ResourceDatabasePopulator dbInit;
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();

        dbInit = new ResourceDatabasePopulator();
        dbInit.addScript(destroyScript);
        dbInit.addScript(initScript);
        dataSourceInitializer.setDatabasePopulator(dbInit);

        dataSourceInitializer.setDataSource(dataSource);

        return dataSourceInitializer;
    }

    @Bean(name="dataSource")
    public DriverManagerDataSource createDriverManagerDataSource(@Value("${driverClassName}") String driverClassName,
                                                                 @Value("${url}") String url,
                                                                 @Value("${usernameDB}") String username,
                                                                 @Value("${passwordDB}") String password
                                                           ) throws java.sql.SQLException{
        Connection connection;
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(driverClassName);
        driverManagerDataSource.setUrl(url);
        driverManagerDataSource.setUsername(username);
        driverManagerDataSource.setPassword(password);

        return driverManagerDataSource;
    }



    @Bean(name="jobRepository")
    public JobRepository createJobRepository(DriverManagerDataSource dataSource,
                                                                   ResourcelessTransactionManager transactionManager,
                                                                   @Value("${databaseType}") String dataBaseType)
            throws  java.lang.Exception{
        JobRepositoryFactoryBean jobRepositoryFactoryBean = new JobRepositoryFactoryBean();
        jobRepositoryFactoryBean.setDataSource(dataSource);
        jobRepositoryFactoryBean.setTransactionManager(transactionManager);
        jobRepositoryFactoryBean.setDatabaseType(dataBaseType);
        return jobRepositoryFactoryBean.getObject();

    }

    @Bean(name="jobLauncher")
    public SimpleJobLauncher createSimpleJobLauncher(JobRepository jobRepository) {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository);
        return simpleJobLauncher;
    }


    @Bean(name="jobBuilderFactory")
    public JobBuilderFactory createJobBuilderFactory(JobRepository jobRepository) {
        return new JobBuilderFactory(jobRepository);
    }


    @Bean(name="stepBuilderFactory")
    public StepBuilderFactory createStepBuilderFactory(JobRepository jobRepository,
                                                       ResourcelessTransactionManager transactionManager) {
        return new StepBuilderFactory(jobRepository,transactionManager);
    }


}

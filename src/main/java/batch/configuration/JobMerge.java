package batch.configuration;


import batch.mapper.CCENTFieldSetMapper;
import batch.mapper.CreditScoreFieldSetMapper;
import batch.model.CreditScoreRecord;
import batch.processor.ItemCCENTProcessor;
import batch.processor.ItemCreditScoreProcessor;
import batch.writer.CreditScoreRequestWriterWithIdCommercial;
import batch.writer.CreditScoreRequestWriterWithNeq;
import batch.writer.CreditScoreRequestWriterWithNeqAndIdCommercial;
import batch.writer.NoOpItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;


@Configuration
@Import(BatchContext.class)
public class JobMerge {

    public static final String CCENTLookup = "CCENTLookup";
    public static final String JOB_MERGE  = "jobMerge";

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    /**
     * Create and configure job
     * @return
     */
    @Bean(name = "jobWithMerging")
    public Job createJobMerge(Step stepLoadCCENT,
                              Step stepBuildRequestCreditScore){
        return jobBuilderFactory.get("JOB_MERGE")
                .start(stepLoadCCENT)
                .next(stepBuildRequestCreditScore)
                .build();
    }

    private FlatFileItemReader initFlatFileItemReader(Resource inResource,
                                                      LineMapper lineMapper) {

        FlatFileItemReader flatFileItemReader = new FlatFileItemReader();
        flatFileItemReader.setResource(inResource);
        flatFileItemReader.setLineMapper(lineMapper);

        /* skip header*/
        flatFileItemReader.setLinesToSkip(1);

        return flatFileItemReader;
    }

    /**
     * Create and configure the steps
     */

    @Bean(name = "stepLoadCCENT")
    public Step createStepLoadCCENT(FlatFileItemReader flatFileCCENTItemReader,
                                    ItemWriter noOpItemWriter,
                                    ItemProcessor itemCCENTProcessor) {
        return stepBuilderFactory.get("stepLoadCCENT")
                .chunk(1)
                .reader(flatFileCCENTItemReader)
                .processor(itemCCENTProcessor)
                .writer(noOpItemWriter)
                .build();
    }

    @Bean(name = "stepBuildRequestCreditScore")
    public Step createStepBuildRequestCreditScore(FlatFileItemReader flatFileCreditScoreItemReader,
                                                  ItemProcessor itemCreditScoreProcessor,
                                                  ItemWriter compositeItemWriterFlatFileRequestCreditScore) {
        return stepBuilderFactory.get("stepBuildRequestCreditScore")
                .chunk(1)
                .reader(flatFileCreditScoreItemReader)
                .processor(itemCreditScoreProcessor)
                .writer(compositeItemWriterFlatFileRequestCreditScore)
                .build();
    }

    /**
     * Create the reader of ccent file
     */
    @Bean(name = "flatFileCCENTItemReader")
    public FlatFileItemReader createFlatFileCCENTItemReader(@Value("${inputResourceCCENT}")Resource inResource,
                                                       LineMapper lineMapperCsvCCENT) {
        return initFlatFileItemReader(inResource,lineMapperCsvCCENT);
    }


    /**
     * Create the reader of CreditScore file
     */
    @Bean(name = "flatFileCreditScoreItemReader")
    public FlatFileItemReader createFlatFileCreditScoreItemReader(@Value("${inputResourceCreditScore}")Resource inResource,
                                                            LineMapper lineMapperCsvCreditScore) {
        return initFlatFileItemReader(inResource,lineMapperCsvCreditScore);
    }


    /**
     * Create the writers of CreditScore file request
     */

    @Bean(name = "noOpItemWriter")
    public NoOpItemWriter createNoOpItemWriter () throws Exception {
        NoOpItemWriter  noOpItemWriter = new NoOpItemWriter();

        return noOpItemWriter;
    }


    @Bean(name = "compositeItemWriterFlatFileRequestCreditScore")
    public CompositeItemWriter createCompositeItemWriterFlatFileRequestCreditScore (FlatFileItemWriter flatFileRequestCreditScoreWriterWithIdCommercial,
                                                                                    FlatFileItemWriter flatFileRequestCreditScoreWriterWithNeq,
                                                                                    FlatFileItemWriter flatFileRequestCreditScoreWriterWithNeqAndIdCommercial) throws Exception {
        CompositeItemWriter compositeItemWriterFlatFileRequestCreditScore = new CompositeItemWriter();

        List<FlatFileItemWriter> listFlatFileitemWriter = new ArrayList<>();
        listFlatFileitemWriter.add(flatFileRequestCreditScoreWriterWithIdCommercial);
        listFlatFileitemWriter.add(flatFileRequestCreditScoreWriterWithNeq);
        listFlatFileitemWriter.add(flatFileRequestCreditScoreWriterWithNeqAndIdCommercial);

        compositeItemWriterFlatFileRequestCreditScore.setDelegates(listFlatFileitemWriter);

        return compositeItemWriterFlatFileRequestCreditScore;
    }


    @Bean(name = "flatFileRequestCreditScoreWriterWithIdCommercial")
    public CreditScoreRequestWriterWithIdCommercial<CreditScoreRecord> createCreditScoreRequesWithIdCommercial (@Value("${outputResourceCreditScore}") Resource outputResource) throws Exception {
        CreditScoreRequestWriterWithIdCommercial<CreditScoreRecord> creditScoreRequestWriter = new CreditScoreRequestWriterWithIdCommercial(outputResource);

        return creditScoreRequestWriter;
    }

    @Bean(name = "flatFileRequestCreditScoreWriterWithNeq")
    public CreditScoreRequestWriterWithNeq<CreditScoreRecord> createCreditScoreRequesWithNeq (@Value("${outputResourceCreditScore}") Resource outputResource) throws Exception {
        CreditScoreRequestWriterWithNeq<CreditScoreRecord> creditScoreRequestWriter = new CreditScoreRequestWriterWithNeq(outputResource);

        return creditScoreRequestWriter;
    }


    @Bean(name = "flatFileRequestCreditScoreWriterWithNeqAndIdCommercial")
    public CreditScoreRequestWriterWithNeqAndIdCommercial<CreditScoreRecord> createCreditScoreRequestWriterWithNeqAndIdCommercial (@Value("${outputResourceCreditScore}") Resource outputResource) throws Exception {
        CreditScoreRequestWriterWithNeqAndIdCommercial<CreditScoreRecord> creditScoreRequestWriter = new CreditScoreRequestWriterWithNeqAndIdCommercial(outputResource);

        return creditScoreRequestWriter;
    }

    /**
     * Customs Entities models , Processors , LineMappers ...
     */


    @Bean(name = "itemCCENTProcessor")
    public ItemCCENTProcessor createCustomItemCCENTProcessor(){
        return new ItemCCENTProcessor();
    }

    @Bean(name = "itemCreditScoreProcessor")
    public ItemCreditScoreProcessor createCreditScoreProcessor(){
        return new ItemCreditScoreProcessor();
    }

    @Bean(name = "lineMapperCsvCCENT")
    public DefaultLineMapper createLineMapperCCENT(DelimitedLineTokenizer lineCCENTTokenizer,
                                                   FieldSetMapper ccENTFieldSetMapper){

        DefaultLineMapper customLineMapper = new DefaultLineMapper();
        customLineMapper.setLineTokenizer(lineCCENTTokenizer);
        customLineMapper.setFieldSetMapper(ccENTFieldSetMapper);

        return customLineMapper;
    }

    @Bean(name = "lineMapperCsvCreditScore")
    public DefaultLineMapper createLineMapperCsvCreditScore(DelimitedLineTokenizer lineCreditScoreTokenizer,
                                                   FieldSetMapper creditScoreFieldSetMapper){

        DefaultLineMapper customLineMapper = new DefaultLineMapper();
        customLineMapper.setLineTokenizer(lineCreditScoreTokenizer);
        customLineMapper.setFieldSetMapper(creditScoreFieldSetMapper);

        return customLineMapper;
    }


    @Bean(name = "lineCCENTTokenizer")
    public DelimitedLineTokenizer createDelimitedLineTokenizer() {
        String[] delimiterValues = new String[] {
                CCENTFieldSetMapper.CCENT_CLE,
                CCENTFieldSetMapper.CCENT_NOM_ENTRP
              };
        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setNames(delimiterValues);
        delimitedLineTokenizer.setDelimiter(";");

        return delimitedLineTokenizer;
    }


    @Bean(name = "ccENTFieldSetMapper")
    public CCENTFieldSetMapper createCCENTFieldSetMapper() {
        CCENTFieldSetMapper ccENTFieldSetMapper = new CCENTFieldSetMapper();
        return ccENTFieldSetMapper;
    }


    @Bean(name = "lineCreditScoreTokenizer")
    public DelimitedLineTokenizer createlineCreditScoreTokenizer() {
        String[] delimiterValues = new String[] {
                CreditScoreFieldSetMapper.NEQ,
                CreditScoreFieldSetMapper.IDCOMMERCIALFILE,
                CreditScoreFieldSetMapper.BUSINESSNAME,
                CreditScoreFieldSetMapper.KEY,
        };
        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setNames(delimiterValues);
        delimitedLineTokenizer.setDelimiter(",");

        return delimitedLineTokenizer;
    }


    @Bean(name = "creditScoreFieldSetMapper")
    public CreditScoreFieldSetMapper createCreditScoreFieldSetMapper() {
        CreditScoreFieldSetMapper creditScoreFieldSetMapper = new CreditScoreFieldSetMapper();
        return creditScoreFieldSetMapper;
    }

}
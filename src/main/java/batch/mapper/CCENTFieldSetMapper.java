package batch.mapper;

import batch.model.CCENTRecord;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class CCENTFieldSetMapper implements FieldSetMapper<CCENTRecord> {

	public static final String CCENT_CLE = "CLE";
	public static final String	CCENT_NOM_ENTRP = "CCENT_NOM_ENTRP";

	@Override
	public CCENTRecord mapFieldSet(FieldSet fieldSet) throws BindException {

		CCENTRecord ccentRecord = new CCENTRecord();

		ccentRecord.setCle(fieldSet.readString(CCENT_CLE));
		ccentRecord.setNomEntreprise(fieldSet.readString(CCENT_NOM_ENTRP));


		return ccentRecord;
		
	}

}
package batch.mapper;

import batch.model.CreditScoreRecord;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class CreditScoreFieldSetMapper implements FieldSetMapper<CreditScoreRecord> {

	public static final String NEQ = "NEQ";
	public static final String	IDCOMMERCIALFILE = "IDCOMMERCIALFILE";
	public static final String	BUSINESSNAME = "BUSINESSNAME";
	public static final String	KEY = "KEY";


	@Override
	public CreditScoreRecord mapFieldSet(FieldSet fieldSet) throws BindException {

		CreditScoreRecord creditScoreRecord = new CreditScoreRecord();

		creditScoreRecord.setNeq(fieldSet.readString(NEQ));
		creditScoreRecord.setIdCommercialeFile(fieldSet.readString(IDCOMMERCIALFILE));
		creditScoreRecord.setBusinessName(fieldSet.readString(BUSINESSNAME));


		return creditScoreRecord;
		
	}

}
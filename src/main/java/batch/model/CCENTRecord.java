package batch.model;

public class CCENTRecord {

	private String cle;
	private String nomEntreprise;

	public String getCle() {
		return cle;
	}

	public void setCle(String cle) {
		this.cle = cle;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	@Override
	public String toString() {
		return "CCENT [cle=" + cle + ", nomEntreprise=" + nomEntreprise + "]";
	}

}
package batch.model;

public class CreditScoreRecord {

	private String neq;
	private String idCommercialeFile;
	private String businessName;

	public String getNeq() {
		return neq;
	}

	public void setNeq(String neq) {
		this.neq = neq;
	}

	public String getIdCommercialeFile() {
		return idCommercialeFile;
	}

	public void setIdCommercialeFile(String idCommercialeFile) {
		this.idCommercialeFile = idCommercialeFile;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}





	@Override
	public String toString() {
		return "CreditInfo [NEQ =" + neq + ", idCommercialeFile =" + idCommercialeFile + ", businessName =" + businessName + "]";
	}

}
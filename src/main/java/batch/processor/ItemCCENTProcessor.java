package batch.processor;

import batch.configuration.JobMerge;
import batch.model.CCENTRecord;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

import java.util.HashMap;

public class ItemCCENTProcessor implements ItemProcessor<CCENTRecord, CCENTRecord> {

	private JobExecution jobExecution;

	private ExecutionContext jobContext;

	private HashMap<String,CCENTRecord> CCENTLookup;

	private int count;

	long startTime, endTime;

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		jobContext = stepExecution.getJobExecution().getExecutionContext();
		CCENTLookup = new HashMap<String,CCENTRecord>();
		jobContext.put(JobMerge.CCENTLookup, CCENTLookup);
		startTime = System.nanoTime();
		count = 0 ;
	}

	@Override
	public CCENTRecord process(CCENTRecord item) throws Exception {

		CCENTLookup.put(item.getCle(),item);

		count++;
		if (count == 10) {
			endTime = System.nanoTime();
			System.out.println("Processing 10 items in " + (endTime - startTime)/1000000 + " ms");
			startTime = System.nanoTime();
		}

		if (count ==  100) {
			endTime = System.nanoTime();
			System.out.println("Processing 100 items in " + (endTime - startTime)/1000000 + " ms");
			startTime = System.nanoTime();
		}


		if (count == 1000) {
			endTime = System.nanoTime();
			System.out.println("Processing 1000 items in " + (endTime - startTime)/1000000 + " ms");
			startTime = System.nanoTime();
		}

		if (count == 10000) {
			endTime = System.nanoTime();
			System.out.println("Processing 10000 items in " + (endTime - startTime)/1000000 + " ms");
			startTime = System.nanoTime();
		}

		if (count == 20000) {
			endTime = System.nanoTime();
			System.out.println("Processing 20000 items in " + (endTime - startTime)/1000000 + " ms");
			startTime = System.nanoTime();
		}


		return item;
	}

}
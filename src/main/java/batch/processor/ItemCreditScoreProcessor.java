package batch.processor;

import batch.configuration.JobMerge;
import batch.model.CCENTRecord;
import batch.model.CreditScoreRecord;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

import java.util.HashMap;

public class ItemCreditScoreProcessor implements ItemProcessor<CreditScoreRecord, CreditScoreRecord> {

	private JobExecution jobExecution;

	private ExecutionContext jobContext;

	private HashMap<String,CCENTRecord> CCENTLookup;

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		jobContext = stepExecution.getJobExecution().getExecutionContext();
		CCENTLookup = (HashMap<String,CCENTRecord>) jobContext.get(JobMerge.CCENTLookup);
	}

	@Override
	public CreditScoreRecord process(CreditScoreRecord item) throws Exception {

		String keyCCENT,businessName;
		StringBuffer strBuf;

		strBuf = new StringBuffer();
		strBuf.append(item.getNeq());
		strBuf.append(item.getIdCommercialeFile());

		keyCCENT = strBuf.toString();
		if (CCENTLookup.containsKey(keyCCENT)) {
			businessName = CCENTLookup.get(keyCCENT).getNomEntreprise();
			businessName=businessName.replaceAll("'","''");
			item.setBusinessName(businessName);
		}

		return item;
	}

}
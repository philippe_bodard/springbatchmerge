package batch.writer;

import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bop017 on 2018-11-02.
 */
public class CreditScoreRequestWriterWithIdCommercial<CreditScoreRecord>   extends FlatFileItemWriter<CreditScoreRecord> {

	BeanWrapperFieldExtractor<CreditScoreRecord> fieldExtractor;
	FormatterLineAggregator<CreditScoreRecord> lineAggregator;


	public CreditScoreRequestWriterWithIdCommercial(Resource outputResource) {

		setResource(outputResource);
		fieldExtractor = new BeanWrapperFieldExtractor<>();
		lineAggregator = new FormatterLineAggregator<>();
		fieldExtractor.setNames(new String[]{"businessName", "idCommercialeFile"});
		fieldExtractor.afterPropertiesSet();
		lineAggregator.setFieldExtractor(fieldExtractor);
		lineAggregator.setFormat("update creditscoreinfo\n" +
				"set businessname = '%s'\n" +
				"where neq is null and idcommercialfile = '%s';\n");
		lineAggregator.setFieldExtractor(fieldExtractor);
		setLineAggregator(lineAggregator);
	}


	public void write(List<? extends CreditScoreRecord> items) throws Exception {

		batch.model.CreditScoreRecord currentCreditScore;

		List<CreditScoreRecord> listItemsWitCommmercialFileOnly;

		listItemsWitCommmercialFileOnly = new ArrayList();

		for ( CreditScoreRecord creditScore : items ) {
			currentCreditScore = (batch.model.CreditScoreRecord) creditScore;
			if (currentCreditScore.getNeq().equals("")) {
				listItemsWitCommmercialFileOnly.add(creditScore);
			}
		}

		super.write(listItemsWitCommmercialFileOnly);

	}

}

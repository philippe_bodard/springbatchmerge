package batch.writer;

import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bop017 on 2018-11-02.
 */
public class CreditScoreRequestWriterWithNeq<CreditScoreRecord>   extends FlatFileItemWriter<CreditScoreRecord> {

	BeanWrapperFieldExtractor<CreditScoreRecord> fieldExtractor;
	FormatterLineAggregator<CreditScoreRecord> lineAggregator;


	public CreditScoreRequestWriterWithNeq(Resource outputResource) {
		setResource(outputResource);
		setAppendAllowed(true);

		fieldExtractor = new BeanWrapperFieldExtractor<>();
		lineAggregator = new FormatterLineAggregator<>();
		fieldExtractor.setNames(new String[]{"businessName", "neq"});
		fieldExtractor.afterPropertiesSet();
		lineAggregator.setFieldExtractor(fieldExtractor);
		lineAggregator.setFormat("update creditscoreinfo\n" +
				"set businessname = '%s'\n" +
				"where neq = '%s' and idcommercialfile is null;\n");
		lineAggregator.setFieldExtractor(fieldExtractor);
		setLineAggregator(lineAggregator);

	}


	public void write(List<? extends CreditScoreRecord> items) throws Exception {

		batch.model.CreditScoreRecord currentCreditScore;

		List<CreditScoreRecord> listItemsWithNeqOnly;

		listItemsWithNeqOnly = new ArrayList();

		for ( CreditScoreRecord creditScore : items ) {
			currentCreditScore = (batch.model.CreditScoreRecord) creditScore;
			if (currentCreditScore.getIdCommercialeFile().equals("")) {
				listItemsWithNeqOnly.add(creditScore);
			}

		}
		super.write(listItemsWithNeqOnly);


	}

}

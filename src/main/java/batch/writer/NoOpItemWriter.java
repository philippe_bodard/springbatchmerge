package batch.writer;

import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Created by bop017 on 2018-11-07.
 */
public class NoOpItemWriter implements ItemWriter {

	@Override
	public void write(List list) throws Exception {
		// no-op
	}
}

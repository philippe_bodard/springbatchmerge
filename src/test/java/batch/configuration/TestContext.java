package batch.configuration;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by philippe local on 2017-09-04.
 */
@Configuration
@PropertySource("classpath:application.properties")
@Import(JobMerge.class)
@EnableBatchProcessing
public class TestContext {


    @Bean
    public JobLauncherTestUtils createSimpleJobTestUtils() {
        return new JobLauncherTestUtils();
    }

}

